/****************************************************************************
 **
 ** This demo file is part of yFiles for JavaFX 2.0.
 **
 ** Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for JavaFX functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for JavaFX version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for JavaFX powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for JavaFX
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
package viewer.graphviewer;

import com.yworks.yfiles.canvas.GraphControl;
import com.yworks.yfiles.canvas.GraphOverviewControl;
import com.yworks.yfiles.canvas.ScrollBarVisibility;
import com.yworks.yfiles.geometry.InsetsD;
import com.yworks.yfiles.graph.FoldingManager;
import com.yworks.yfiles.graph.GraphItemTypes;
import com.yworks.yfiles.graph.IGraph;
import com.yworks.yfiles.graph.IMapperRegistry;
import com.yworks.yfiles.graph.INode;
import com.yworks.yfiles.graphml.GraphMLIOHandler;
import com.yworks.yfiles.input.ClickEventArgs;
import com.yworks.yfiles.input.GraphCommands;
import com.yworks.yfiles.input.GraphEditorInputMode;
import com.yworks.yfiles.input.GraphViewerInputMode;
import com.yworks.yfiles.input.IInputMode;
import com.yworks.yfiles.input.NavigationInputMode;
import com.yworks.yfiles.input.QueryItemToolTipEventArgs;
import com.yworks.yfiles.model.IMapper;
import com.yworks.yfiles.model.IModelItem;
import com.yworks.yfiles.support.ItemInputEventArgs;
import com.yworks.yfiles.support.ModifierKeys;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tooltip;
import javafx.scene.web.WebView;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * The main controller for this demo.
 * It manages the different views on the graph including information about single nodes.
 */
public class GraphViewerController {
  public GraphControl graphControl;
  public ComboBox<String> graphChooserBox;
  public Button previousButton;
  public Button nextButton;
  public Label graphDescriptionLabel;
  public GraphOverviewControl graphOverviewControl;
  public WebView webView;
  public Label nodeLabelTextBlock;
  public Label nodeDescriptionTextBlock;
  public Hyperlink nodeUrlButton;

  /**
   * Initializes the controller. This is called when the FXMLLoader instantiates the scene graph.
   * At the time this method is called, all nodes in the scene graph is available. Most importantly,
   * the GraphControl instance is initialized.
   */
  public void initialize() {
    // enable folding
    final FoldingManager manager = new FoldingManager();
    // replace the displayed graph with a managed view
    graphControl.setGraph(manager.createManagedView().getGraph());

    final IGraph graph = graphControl.getGraph();

    graphControl.setFileOperationsEnabled(true);

    // add mappers for additional data
    final IMapperRegistry masterRegistry = graph.getFoldedGraph().getManager().getMasterGraph().getMapperRegistry();
    masterRegistry.addDictionaryMapper(INode.class, String.class, "ToolTip");
    masterRegistry.addDictionaryMapper(INode.class, String.class, "Description");
    masterRegistry.addDictionaryMapper(INode.class, String.class, "Url");
    masterRegistry.addDictionaryMapper(IGraph.class, String.class, "GraphDescription");

    graphControl.currentItemProperty().addListener((observableValue, iModelItem, iModelItem2) -> onCurrentItemChanged());

    // set up viewer input modes
    final GraphViewerInputMode graphViewerInputMode = new GraphViewerInputMode();
    graphViewerInputMode.setToolTipItems(GraphItemTypes.LABELED_ITEM);
    graphViewerInputMode.setClickableItems(GraphItemTypes.NODE);
    graphViewerInputMode.setFocusableItems(GraphItemTypes.NODE);
    graphViewerInputMode.setSelectableItems(GraphItemTypes.ALL);
    graphViewerInputMode.setMarqueeSelectableItems(GraphItemTypes.ALL);

    graphViewerInputMode.addQueryItemToolTipListener(this::onQueryItemToolTip);
    graphViewerInputMode.addItemClickedListener(this::onItemClicked);

    final NavigationInputMode navigationInputMode = graphViewerInputMode.getNavigationInputMode();
    navigationInputMode.setCollapsingGroupsAllowed(true);
    navigationInputMode.setExpandingGroupsAllowed(true);
    navigationInputMode.setUsingCurrentItemForCommandsEnabled(true);
    navigationInputMode.setFittingContentAfterGroupActionsEnabled(false);

    graphViewerInputMode.getClickInputMode().addClickedListener(this::onClickInputModeOnClicked);
    
    // initialize controls
    //graphControl.setInputMode(graphViewerInputMode);
    graphControl.setInputMode(createEditorMode());

   graphChooserBox.getItems().addAll("social-network");

    graphOverviewControl.setGraphControl(graphControl);
    graphOverviewControl.setHorizontalScrollBarPolicy(ScrollBarVisibility.NEVER);
    graphOverviewControl.setVerticalScrollBarPolicy(ScrollBarVisibility.NEVER);

   //webView.getEngine().load(getClass().getResource("resources/help.html").toExternalForm());

    graphControl.fitGraphBounds();
  }
  
   /**
   * Creates the default input mode for the GraphControl, a {@link GraphEditorInputMode}.
   * @return a new GraphEditorInputMode instance
   */
  private IInputMode createEditorMode() {
    GraphEditorInputMode mode = new GraphEditorInputMode();

    // we enable label editing
    mode.setLabelEditingAllowed(false);
    mode.setNodeCreationAllowed(false);
    mode.setSelectableItems(GraphItemTypes.NODE);
    mode.setMarqueeSelectableItems(GraphItemTypes.NODE);
    mode.setMovableItems(GraphItemTypes.ALL);
    return mode;
  }

  /**
   * Called when stage is shown because it needs the scene to be ready.
   */

  public void onLoaded() {
    graphChooserBox.getSelectionModel().select(0);
  }

  /**
   * Updates all description when another element of the graph gets focused.
   */
  private void onCurrentItemChanged() {
    final IModelItem currentItem = graphControl.getCurrentItem();
    if (currentItem instanceof INode) {
      final INode node = (INode) currentItem;
      nodeDescriptionTextBlock.setText(getDescriptionMapper().getItem(node));
      nodeLabelTextBlock.setText(node.getLabels().size() > 0 ? node.getLabels().getItem(0).getText() : "");
      final String url = getUrlMapper().getItem(node);
      if (url != null) {
        nodeUrlButton.setText(url);
        nodeUrlButton.setDisable(false);
      } else {
        nodeUrlButton.setText("");
        nodeUrlButton.setDisable(true);
      }
    } else {
      nodeDescriptionTextBlock.setText("");
      nodeLabelTextBlock.setText("");
      nodeUrlButton.setText("");
      nodeUrlButton.setDisable(true);
    }
  }

  /**
   * Shows a tooltip for a node that is hovered for a certain amount of time.
   */
  private void onQueryItemToolTip(Object sender, QueryItemToolTipEventArgs<IModelItem> queryItemToolTipEventArgs) {
    if (queryItemToolTipEventArgs.getItem() instanceof INode && !queryItemToolTipEventArgs.isHandled()) {
      final INode node = (INode) queryItemToolTipEventArgs.getItem();
      final IMapper<INode, String> descriptionMapper = getDescriptionMapper();
      final String toolTipText = getToolTipMapper().getItem(node) != null
          ? getToolTipMapper().getItem(node)
          : (descriptionMapper != null ? descriptionMapper.getItem(node) : null);
      if (toolTipText != null) {
        queryItemToolTipEventArgs.setToolTip(new Tooltip(toolTipText));
        queryItemToolTipEventArgs.setHandled(true);
      }
    }
  }

  /**
   * Shows the content of a group or a dialog with information about the current node depending on what modifier was
   * pressed.
   */
  private void onItemClicked(Object sender, ItemInputEventArgs<IModelItem> event) {
      if (event.getItem() instanceof INode) {
        graphControl.setCurrentItem(event.getItem());
        if ((graphControl.getLastMouse2DEvent().getModifierState().value()
            & (ModifierKeys.SHIFT.value() | ModifierKeys.CONTROL.value())) == (ModifierKeys.SHIFT.value() | ModifierKeys.CONTROL.value())) {
          if (GraphCommands.ENTER_GROUP_COMMAND.canExecute(event.getItem(), graphControl)) {
            GraphCommands.ENTER_GROUP_COMMAND.execute(event.getItem(), graphControl);
            event.setHandled(true);
          }
        }
      }
    }

  /**
   * Returns from the content of a group/folder to the outer view when clicking on void space in the graph while
   * pressing modifiers shift and control.
   */
  private void onClickInputModeOnClicked(Object sender, ClickEventArgs args) {
    if (!graphControl.getGraphModelManager().enumerateHits(args.getLocation()).moveNext()) { // nothing hit
      if ((args.getModifierState().value() & (ModifierKeys.SHIFT.value() | ModifierKeys.CONTROL.value()))
          == (ModifierKeys.SHIFT.value() | ModifierKeys.CONTROL.value())) {
        if (GraphCommands.EXIT_GROUP_COMMAND.canExecute(null, graphControl) && !args.isHandled()) {
          GraphCommands.EXIT_GROUP_COMMAND.execute(null, graphControl);
          args.setHandled(true);
        }
      }
    }
  }

  /**
   * Reads a sample graph from a graphml file according currently selected entry in the graph chooser box.
   */
  private void readSampleGraph() {
    try {
      String fileName = String.format("resources/%s.graphml", graphChooserBox.getSelectionModel().getSelectedItem());
      graphControl.getGraph().clear();
      fileName = "resources/social-network.graphml";
      final GraphMLIOHandler ioHandler = new GraphMLIOHandler();
      ioHandler.addRegistryInputMapper(INode.class, String.class, "Description");
      ioHandler.addRegistryInputMapper(INode.class, String.class, "ToolTip");
      ioHandler.addRegistryInputMapper(INode.class, String.class, "Url");
      ioHandler.addRegistryInputMapper(IGraph.class, String.class, "GraphDescription");
      ioHandler.read(graphControl.getGraph(), getClass().getResource(fileName).toExternalForm());

      graphDescriptionLabel.setText(
          getGraphDescriptionMapper().getItem(graphControl.getGraph().getFoldedGraph().getManager().getMasterGraph()));

      graphControl.fitGraphBounds(new InsetsD(10));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Updates the current graph and the buttons in the toolbar when the selected graph changes.
   */
  public void graphChanged() {
    readSampleGraph();
    //updateButtons();
  }

  /**
   * Selects the previous graph in the list and updates the buttons in the toolbar.
   */

  public void previousButtonClicked() {
    final SingleSelectionModel<String> selectionModel = graphChooserBox.getSelectionModel();
    selectionModel.select(selectionModel.getSelectedIndex() - 1);
    updateButtons();
  }

  /**
   * Selects the next graph in the list and updates the buttons in the toolbar.
   */
 
  public void nextButtonClicked() {
    final SingleSelectionModel<String> selectionModel = graphChooserBox.getSelectionModel();
    selectionModel.select(selectionModel.getSelectedIndex() + 1);
    updateButtons();
  }

  /**
   * Updates the toolbar buttons. Buttons that would leave the range of samples are disabled.
   */

  private void updateButtons() {
    final SingleSelectionModel<String> selectionModel = graphChooserBox.getSelectionModel();
    nextButton.setDisable(selectionModel.getSelectedIndex() == graphChooserBox.getItems().size() - 1);
    previousButton.setDisable(selectionModel.getSelectedIndex() == 0);
  }

  /**
   * Opens a browser with the current url when the {@link Hyperlink} gets clicked.
   */
  public void nodeUrlLinkClicked() {
    try {
      Desktop.getDesktop().browse(new URI(nodeUrlButton.getText()));
    } catch (IOException | URISyntaxException e) {
      e.printStackTrace();
    }
  }

  /**
   * Returns an {@link IMapper} that returns a graph description text or <code>null</code> for a key.
   */
  private IMapper<IGraph, String> getGraphDescriptionMapper() {
    return graphControl.getGraph().getMapperRegistry().getMapper(IGraph.class, String.class, "GraphDescription");
  }

  /**
   * Returns an {@link IMapper} that returns a description text or <code>null</code> for a key.
   */
  private IMapper<INode, String> getDescriptionMapper() {
    return graphControl.getGraph().getMapperRegistry().getMapper(INode.class, String.class, "Description");
  }

  /**
   * Returns an {@link IMapper} that returns a tooltip text or <code>null</code> for a key.
   */
  private IMapper<INode, String> getToolTipMapper() {
    return graphControl.getGraph().getMapperRegistry().getMapper(INode.class, String.class, "ToolTip");
  }

  /**
   * Returns an {@link IMapper} that returns an url string or <code>null</code> for a key.
   */
  private IMapper<INode, String> getUrlMapper() {
    return graphControl.getGraph().getMapperRegistry().getMapper(INode.class, String.class, "Url");
  }
}
