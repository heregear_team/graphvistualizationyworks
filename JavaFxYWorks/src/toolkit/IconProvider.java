/****************************************************************************
 **
 ** This demo file is part of yFiles for JavaFX 2.0.
 **
 ** Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for JavaFX functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for JavaFX version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for JavaFX powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for JavaFX
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
package toolkit;

import com.yworks.yfiles.input.GraphCommands;
import com.yworks.yfiles.system.ApplicationCommands;
import com.yworks.yfiles.system.NavigationCommands;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides some often used icons
 */
public class IconProvider {

  private static IconProvider INSTANCE = new IconProvider();

  public static Image NEW = new Image(getResource("new-document-16.png"));
  public static Image COPY = new Image(getResource("copy-16.png"));
  public static Image CUT = new Image(getResource("cut-16.png"));
  public static Image PASTE = new Image(getResource("paste-16.png"));
  public static Image DELETE = new Image(getResource("delete3-16.png"));
  public static Image EXPORT_IMAGE = new Image(getResource("export-image-16.png"));
  public static Image RELOAD = new Image(getResource("reload-16.png"));
  public static Image GRID = new Image(getResource("grid-16.png"));
  public static Image GROUP = new Image(getResource("group-16.png"));
  public static Image OPEN = new Image(getResource("open-16.png"));
  public static Image ORTHOGONAL_EDITING = new Image(getResource("orthogonal-editing-16.png"));
  public static Image PRINT = new Image(getResource("print-16.png"));
  public static Image REDO = new Image(getResource("redo-16.png"));
  public static Image UNDO = new Image(getResource("undo-16.png"));
  public static Image SAVE = new Image(getResource("save-16.png"));
  public static Image SNAPPING = new Image(getResource("snap-16.png"));
  public static Image UNGROUP = new Image(getResource("ungroup-16.png"));
  public static Image ZOOM_FIT = new Image(getResource("fit2-16.png"));
  public static Image ZOOM_IN = new Image(getResource("plus2-16.png"));
  public static Image ZOOM_OUT = new Image(getResource("minus2-16.png"));
  public static Image ZOOM_RESET = new Image(getResource("zoom-original2-16.png"));
  public static Image LAYOUT_HIERARCHIC = new Image(getResource("layout-hierarchic.png"));
  public static Image LAYOUT_ORGANIC = new Image(getResource("layout-organic-16.png"));
  public static Image LAYOUT_ORTHOGONAL = new Image(getResource("layout-orthogonal-16.png"));
  public static Image NEXT = new Image(getResource("rightarrow.png"));
  public static Image PREVIOUS = new Image(getResource("leftarrow.png"));
  public static Image UP = new Image(getResource("uparrow.png"));
  public static Image DOWN = new Image(getResource("downarrow.png"));
  public static Image PLUS = new Image(getResource("plus-16.png"));
  public static Image MINUS = new Image(getResource("minus-16.png"));

  private static Map<String, Image> names = new HashMap<>();
  static {
    names.put(ApplicationCommands.NEW_COMMAND.getName(), NEW);
    names.put(ApplicationCommands.COPY_COMMAND.getName(), COPY);
    names.put(ApplicationCommands.CUT_COMMAND.getName(), CUT);
    names.put(ApplicationCommands.PASTE_COMMAND.getName(), PASTE);
    names.put(ApplicationCommands.DELETE_COMMAND.getName(), DELETE);
    names.put("RELOAD", RELOAD);
    names.put("EXPORT_IMAGE", EXPORT_IMAGE);
    names.put("GRID", GRID);
    names.put(GraphCommands.GROUP_SELECTION_COMMAND.getName(), GROUP);
    names.put(ApplicationCommands.OPEN_COMMAND.getName(), OPEN);
    names.put("ORTHOGONAL_EDITING", ORTHOGONAL_EDITING);
    names.put(ApplicationCommands.PRINT_COMMAND.getName(), PRINT);
    names.put(ApplicationCommands.REDO_COMMAND.getName(), REDO);
    names.put(ApplicationCommands.UNDO_COMMAND.getName(), UNDO);
    names.put(ApplicationCommands.SAVE_COMMAND.getName(), SAVE);
    names.put("SNAPPING", SNAPPING);
    names.put(GraphCommands.UNGROUP_SELECTION_COMMAND.getName(), UNGROUP);
    names.put("ZOOM_FIT", ZOOM_FIT);
    names.put("ZOOM_RESET", ZOOM_RESET);
    names.put(NavigationCommands.INCREASE_ZOOM_COMMAND.getName(), ZOOM_IN);
    names.put(NavigationCommands.DECREASE_ZOOM_COMMAND.getName(), ZOOM_OUT);
    names.put("LAYOUT_HIERARCHIC", LAYOUT_HIERARCHIC);
    names.put("LAYOUT_ORGANIC", LAYOUT_ORGANIC);
    names.put("LAYOUT_ORTHOGONAL", LAYOUT_ORTHOGONAL);
    names.put("NEXT", NEXT);
    names.put("PREVIOUS", PREVIOUS);
    names.put("UP", UP);
    names.put("DOWN", DOWN);
    names.put("PLUS", PLUS);
    names.put("MINUS", MINUS);
  }
  
  private static InputStream getResource(String name) {
    return INSTANCE.getClass().getResourceAsStream("/toolkit/resources/" + name);
  }

  public static ImageView valueOf(String name){
    Image img = names.get(name);
    if (img == null){
      throw new IllegalArgumentException("Icon not found: "+name);
    }
    return new ImageView(img);
  }
}
