/****************************************************************************
 **
 ** This demo file is part of yFiles for JavaFX 2.0.
 **
 ** Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for JavaFX functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for JavaFX version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for JavaFX powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for JavaFX
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
package toolkit;

import com.yworks.yfiles.canvas.CanvasControl;
import com.yworks.yfiles.geometry.RectD;
import com.yworks.yfiles.io.PixelImageExporter;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Helper class to provide simple bitmap export.
 */
public class BitmapExportHelper {

  private BitmapExportHelper() {
  }

  /**
   * Convenience method that exports the {@link com.yworks.yfiles.canvas.CanvasControl#contentRect} of a control to an
   * image in a stream. For full control over the output use the {@link com.yworks.yfiles.io.PixelImageExporter}.
   *
   * @param canvas The control whose content shall be written.
   * @param file   The name of the file to write the image to.
   */
  public static void exportToBitmap(CanvasControl canvas, String file) throws IOException {
    FileOutputStream stream = new FileOutputStream(file);
    exportToBitmap(canvas, stream, "image/" + file.substring(file.lastIndexOf(".") + 1), canvas.getContentRect());
    stream.close();
  }

  /**
   * Convenience method that exports the {@link CanvasControl#contentRect} of a control to an image in a stream. For
   * full control over the output use the {@link com.yworks.yfiles.io.PixelImageExporter}.
   *
   * @param canvas    The control whose content shall be written.
   * @param stream    The stream to write the image to.
   * @param format    The output format to use. This must be one of <code>"image/jpeg"</code>, <code>"image/png"</code>
   *                  or <code>"image/bmp"</code>
   * @param worldRect The rectangle in the world coordinate system to export.
   */
  public static void exportToBitmap(CanvasControl canvas, OutputStream stream, String format, RectD worldRect)
      throws IOException {
    PixelImageExporter exporter = new PixelImageExporter(worldRect);

    WritableImage image = exporter.createImage(canvas);

    BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);

    bufferedImage = preProcess(bufferedImage, format);

    String extension = format.substring("image/".length());

    ImageIO.write(bufferedImage, extension, stream);
  }

  /*
   * Pre-processes the image dependent on the output format.
   */
  private static BufferedImage preProcess(BufferedImage image, String format) {

    // jpg and bmp do not support alpha
    if ("image/jpeg".equals(format) || "image/jpe".equals(format)
        || "image/jpg".equals(format) || "image/bmp".equals(format)) {
      BufferedImage temp = image;

      final int width = image.getWidth();
      final int height = image.getHeight();

      image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

      final ColorModel colorModel = image.getColorModel();
      for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
          final int rgb = temp.getRGB(x, y);
          int newRgb = ((colorModel.getRed(rgb) & 0xFF) << 16) |
              ((colorModel.getGreen(rgb) & 0xFF) << 8) |
              ((colorModel.getBlue(rgb) & 0xFF));
          image.setRGB(x, y, newRgb);
        }
      }
    }

    return image;
  }
}
