/****************************************************************************
 **
 ** This demo file is part of yFiles for JavaFX 2.0.
 **
 ** Copyright (c) 2000-2015 by yWorks GmbH, Vor dem Kreuzberg 28,
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ** yFiles demo files exhibit yFiles for JavaFX functionalities. Any redistribution
 ** of demo files in source code or binary form, with or without
 ** modification, is not permitted.
 **
 ** Owners of a valid software license for a yFiles for JavaFX version that this
 ** demo is shipped with are allowed to use the demo source code as basis
 ** for their own yFiles for JavaFX powered applications. Use of such programs is
 ** governed by the rights and conditions as set out in the yFiles for JavaFX
 ** license agreement.
 **
 ** THIS SOFTWARE IS PROVIDED ''AS IS'' AND ANY EXPRESS OR IMPLIED
 ** WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 ** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 ** NO EVENT SHALL yWorks BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 ** TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 ** PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 ** LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 ** NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 ** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 **
 ***************************************************************************/
package toolkit;

import com.yworks.util.EventArgs;
import com.yworks.yfiles.system.Command;
import com.yworks.yfiles.system.KeyCodeCombinationCollection;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;

/**
 * A MenuItem that automatically wires up the Command and
 * initializes and updates it's visual representation via {@link IconProvider}.
 * It has javafx properties {@link #commandProperty()} and {@link #commandTargetProperty()}
 * to make it easily constructable via FXML. Example:
 * <p>
 *   <code><xmp><CommandMenuItem command="IncreaseZoom" commandTarget="${graphControl}"/></xmp></code>
 * </p>
 * <p>
 *  This constructs a CommandMenuItem that has the
 *  {@link com.yworks.yfiles.system.NavigationCommands#INCREASE_ZOOM_COMMAND}
 *  command and the icon provided by {@link IconProvider#ZOOM_IN}.
 *  The graphControl is a {@link com.yworks.yfiles.canvas.GraphControl} defined in
 *  FXML via <code><xmp><GraphControl fx:id="graphControl"/></xmp></code>
 * </p>
 */
public class CommandMenuItem extends MenuItem {

  private ObjectProperty<Command> command = new SimpleObjectProperty<>(this, "command");
  private CanExecuteChangedHandler canExecuteChangedEvent;
  private CommandMenuItem.CanExecuteChangeListener canExecuteChangeListener;

  public ObjectProperty<Command> commandProperty(){
    return command;
  }

  public Command getCommand(){
    return command.get();
  }

  public void setCommand (final Command command){
    if (command != null){
      // remove listener of old command if needed
      Command oldCommand = getCommand();
      if (oldCommand != null){
        oldCommand.removeCanExecuteChangedListener(canExecuteChangedEvent);
      }
      command.addCanExecuteChangedListener(canExecuteChangedEvent);
      commandParameterProperty().addListener(canExecuteChangeListener);
      commandTargetProperty().addListener(canExecuteChangeListener);
    }

    this.command.set(command);

    KeyCodeCombinationCollection inputGestures = getCommand().getInputGestures();
    if (inputGestures != null && !inputGestures.isEmpty()) {
      setAccelerator(inputGestures.get(0));
    }

    if (getText() == null) {
      this.setText(getCommand().getText());
    }
  }

  // command parameter property: anything (type Object)

  private ObjectProperty<Object> commandParameter = new SimpleObjectProperty<>(this, "commandParameter");

  public ObjectProperty<Object> commandParameterProperty(){
    return commandParameter;
  }

  public Object getCommandParameter(){
    return commandParameter.get();
  }

  public void setCommandParameter (Object commandParameter){
    this.commandParameter.set(commandParameter);
  }

  // command target property: a Control (most commonly a CanvasControl)

  private ObjectProperty<Control> commandTarget = new SimpleObjectProperty<>(this, "commandTarget");

  public ObjectProperty<Control> commandTargetProperty(){
    return commandTarget;
  }

  public Control getCommandTarget(){
    return commandTarget.get();
  }

  public void setCommandTarget (Control commandTarget){
    this.commandTarget.set(commandTarget);
  }

  public CommandMenuItem() {
    super();
    canExecuteChangedEvent = new CanExecuteChangedHandler();
    canExecuteChangeListener = new CanExecuteChangeListener();
    setOnAction(actionEvent -> {
      Command command1 = getCommand();
      Control target = getCommandTarget();
      Object parameter = getCommandParameter();
      if (command1.canExecute(parameter, target)){
        command1.execute(parameter, target);
      }
    });
  }

  /**
   * Listens to changes on canExecute of the command and disables or enables the button accordingly.
   */
  private class CanExecuteChangedHandler implements com.yworks.util.EventHandler {

    @Override
    public void onEvent(Object source, EventArgs args) {
      CommandMenuItem.this.setDisable(!getCommand().canExecute(getCommandParameter(), getCommandTarget()));
    }
  }

  /**
   * Listens to changes on canExecute of the command and disables or enables the button accordingly.
   */
  private class CanExecuteChangeListener implements ChangeListener {

    @Override
    public void changed(ObservableValue observableValue, Object o, Object o2) {
      canExecuteChangedEvent.onEvent(null, null);
    }
  }
}
